<?php
$_POST = json_decode(file_get_contents('php://input'), true);

if (isset($_POST)) {
    $data['arrs'] = array(
        'id' => $_POST['id'],
        'title' => $_POST['title'],
        'price' => $_POST['price'],
    );
    echo json_encode($data['arrs']);
} else {
    $data['arrs'] = array(
        'id' => 2,
        'title' => 'test2',
        'price' => '200',
    );
    echo json_encode($data['arrs']);
}